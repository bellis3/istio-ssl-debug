# Create cluster
```
minikube start
```

# Install Istio w/ Ingress Gateway
```
helm repo add istio https://istio-release.storage.googleapis.com/charts
helm repo update
kubectl create namespace istio-system
helm install istio-base istio/base -n istio-system
helm install istiod istio/istiod -n istio-system --wait
kubectl create namespace istio-ingress
kubectl label namespace istio-ingress istio-injection=enabled
helm install istio-ingress istio/gateway -n istio-ingress
kubectl label namespace default istio-injection=enabled
```

# Issue a certificate and create a secret
```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 365 -subj '/CN=test.localhost' -nodes
kubectl create secret tls tls-secret -n istio-ingress \
  --cert=cert.pem \
  --key=key.pem
```

# Install Sample Application
```
kubectl apply -f sample.yml
```

# Tunnel (run this in a separate shell)
```
minikube update-context
minikube tunnel
```

# Add this line to your host file
```
127.0.0.1 test.localhost
```

Now go to your browser at "https://test.localhost". What do you see?

Try to do a `curl https://test.localhost:443 -v`

What happens when you do `openssl s_client -connect  test.localhost:443 -msg`?
